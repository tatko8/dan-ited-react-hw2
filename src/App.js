import React from "react";
import { useState, useEffect } from "react";
import Modal from "./components/Modals/Modal";
import Wrapper from "./components/Wrapper/Wrapper";
import Main from "./components/Main/Main";
import axios from "axios";
import Header from "./components/Header/Header";

export const ItemsSelectedInHeader = React.createContext();
export const AddToCartBtnFunc = React.createContext();
export const AddOrRemoveToFavouritesBtnFunc = React.createContext();

function App() {
  const [amountOfProducts, setProductsToTheCart] = useState(
    Number(localStorage.getItem("amountOfProducts") || 0)
  );
  const [amountOfFavourites, setToFavourites] = useState(
    Number(localStorage.getItem("amountOfFavourites") || 0)
  );
  const [modal, setModal] = useState(false);
  const [closeBtnForModal, setCloseBtnForModal] = useState(true);
  const [products, setProducts] = useState([]);

  const addProductToTheCart = () => {
    setProductsToTheCart((prev) => prev + 1);
  };

  const closeModalWindow = () => {
    setModal(false);
  };
  const AddToFavourites = () => {
    setToFavourites((prev) => prev + 1);
  };
  const RemoveFromFavourites = () => {
    setToFavourites((prev) => prev - 1);
  };

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios
        .get(`${document.location.href}books.json`)
        .catch((err) => {
          console.warn(err);
          alert("An error occurred. Try again later.");
        });
      setProducts(response.data);
    };
    fetchData();
  }, []);

  useEffect(() => {
    localStorage.setItem("amountOfProducts", amountOfProducts);
  }, [amountOfProducts]);

  useEffect(() => {
    localStorage.setItem("amountOfFavourites", amountOfFavourites);
  }, [amountOfFavourites]);
  return (
    <>
      <Wrapper>
        <Modal
          active={modal}
          closeModal={closeModalWindow}
          header="Add this item to the cart?"
          textContent="Are you sure you want to add this item to the cart? "
          shouldClose={closeBtnForModal}
          closeModalWindow={closeModalWindow}
          addProductToTheCart={addProductToTheCart}
        />

        <ItemsSelectedInHeader.Provider
          value={{ amountOfProducts, amountOfFavourites }}
        >
          <Header />
        </ItemsSelectedInHeader.Provider>

        <AddOrRemoveToFavouritesBtnFunc.Provider
          value={{ AddToFavourites, RemoveFromFavourites }}
        >
          <AddToCartBtnFunc.Provider
            value={() => {
              setModal(true);
            }}
          >
            <Main products={products} />
          </AddToCartBtnFunc.Provider>
        </AddOrRemoveToFavouritesBtnFunc.Provider>
      </Wrapper>
    </>
  );
}

export default App;
