import "./Modal.scss";
import ActionBtnsForModal from "./ActionBtn/ActionBtn";
import PropTypes from "prop-types";
function Modal({
  active,
  closeModal,
  header,
  textContent,
  shouldClose,
  closeModalWindow,
  addProductToTheCart,
}) {
  return (
    <div className={active ? "modal active" : "modal"} onClick={closeModal}>
      <div className="modal_content" onClick={(e) => e.stopPropagation()}>
        <div
          className={
            shouldClose ? "modal-closeButton toClose" : "modal-closeButton"
          }
          onClick={closeModal}
        >
          X
        </div>
        <h2 className="modal-header">{header}</h2>
        <p className="modalTextContent">{textContent}</p>

        <ActionBtnsForModal
          closeModalWindow={closeModalWindow}
          addProductToTheCart={addProductToTheCart}
        />
      </div>
    </div>
  );
}

Modal.propTypes = {
  active: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  textContent: PropTypes.string.isRequired,
  shouldClose: PropTypes.bool.isRequired,
};
export default Modal;

