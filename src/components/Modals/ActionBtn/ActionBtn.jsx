import "./ActionBtn.scss";

function ActionBtnsForModal({ closeModalWindow, addProductToTheCart }) {
  return (
    <div className="btnsForModal">
      <button
        className="actionForModal submit"
        onClick={() => {
          closeModalWindow();
          addProductToTheCart();
        }}
      >
        OK
      </button>
      <button
        className="actionForModal cancel"
        onClick={() => {
          closeModalWindow();
        }}
      >
        Cancel
      </button>
    </div>
  );
}
export default ActionBtnsForModal;

