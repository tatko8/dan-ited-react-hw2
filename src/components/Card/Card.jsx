import "./Card.scss";
import FavoriteIcon from "./Fav/Fav";
import PropTypes from "prop-types";
import Button from "../Buttons/Button";

const Card = ({ image, name, price, vin }) => {
  return (
    <div className="card">
      <li className="card__item">
        <div className="card__image">
          <img className="card__image" src={image}></img>
        </div>
        <p className="card__name"> {name} </p>
        <p className="card__article"> Article: {vin} </p>
        <p className="card__price">
          {new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "UAH",
            currencyDisplay: "narrowSymbol",
          }).format(price)}
        </p>
        <div className="card__action-btns">
          <Button backgroundColor="#3f3a03" text="Add to cart" />
          <FavoriteIcon />
        </div>
      </li>
    </div>
  );
};

Card.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  vin: PropTypes.number.isRequired,
};

export default Card;
