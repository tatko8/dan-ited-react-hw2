import "./Main.scss";
import Card from "../Card/Card";
import PropTypes from "prop-types";

const MainSection = ({ products }) => {
  return (
    <div className="main-section">
      <ul className="main-section__list">
        {products.map((product) => (
          <Card
            key={product.id}
            name={product.name}
            price={product.price}
            image={product.image}
            vin={product.vin}
          />
        ))}
      </ul>
    </div>
  );
};

MainSection.propTypes = {
  products: PropTypes.array.isRequired,
};
export default MainSection;
