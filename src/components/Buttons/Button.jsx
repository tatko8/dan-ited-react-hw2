import { useContext } from "react";
import "./Button.scss";
import PropTypes from "prop-types";
import { AddToCartBtnFunc } from "../../App";

function Button({ backgroundColor = "rgb(116, 34, 50)", text }) {
  const btnHandler = useContext(AddToCartBtnFunc);

  const bgColor = {
    backgroundColor: backgroundColor,
  };

  return (
    <button onClick={btnHandler} className="button" style={bgColor}>
      {text}
    </button>
  );
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default Button;

