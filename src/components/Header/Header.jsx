import CartIconForHeader from "./HeaderBtn/HeaderBtnCart/Cart";
import FavouriteIconForHeader from "./HeaderBtn/HeaderBtnFav/FavBtn";
import "./Header.scss";

function Header() {
  return (
    <div className="header">
      <div className="header__icons">
        <CartIconForHeader />
        <FavouriteIconForHeader />
      </div>
    </div>
  );
}
export default Header;
